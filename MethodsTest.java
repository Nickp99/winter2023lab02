public class MethodsTest{
	public static void main(String[] args){
		int x=5;
		System.out.println(x);
		methodNoInputNoReturn();
		System.out.println(x);
		System.out.println(x);
		methodOneInputNoReturn(x+10);
		System.out.println(x);
		methodTwoInputNoReturn(x,3.5);
		int z = methodNoInputReturnInt();
		System.out.println("I m the return method "+z);
		double root =sumSquareRoot(9 , 5);
		System.out.println(root);
		String s1="java";
		String s2="programming";
		System.out.println(s1.length());
		System.out.println(s2.length());
		System.out.println(SecondClass.addOne(50));
		SecondClass sc = new SecondClass();
		System.out.println(sc.addTwo(50));
	} 
	public static  void methodNoInputNoReturn(){
		System.out.println("I'm in a method that takes no input and returns nothing");
		int x =20;
		System.out.println(x);
	}
	public static void methodOneInputNoReturn(double p){
		p= p-5;
		System.out.println("Inside the method  one input no returns");
		System.out.println(p);
		
	}
	public static void methodTwoInputNoReturn(int x, double d){
		System.out.println("Method 2");
		System.out.println(x);
		System.out.println(d);
	}
	public static int methodNoInputReturnInt(){
		int x =5;
		return x;
	}
	public static double sumSquareRoot(int x, int y){
		double root = x+y;
		root = Math.sqrt(root);
		return root;
	}
}